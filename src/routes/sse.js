const express = require('express');

const router = express.Router();

const userChannelRes = {};

router.get('/user/:id', (req, res) => {
  const { id } = req.params;

  res.setHeader('Content-Type', 'text/event-stream');
  res.setHeader('Cache-Control', 'no-cache');
  res.setHeader('Connection', 'keep-alive');

  // 推送 connected 事件
  res.write('event: connected\n');
  res.write('data: {"message": "Hello, client!"}\n\n');

  // 儲存response
  userChannelRes[id] = res;

  // 當客戶端斷開連接時，停止推送事件
  req.on('close', () => {
    delete userChannelRes[id];
  });
});

router.post('/', (req, res) => {
  const { id, event, data } = req.body;
  const sseResponse = userChannelRes[id];
  const responseData = typeof data === 'object' ? JSON.stringify(data) : data;

  if (event) {
    sseResponse.write(`event: ${event}\n`);
  }
  sseResponse.write(`data: ${responseData}\n\n`);

  res.send();
  return;
});

module.exports = router;
