const express = require('express');

const routes = require('./routes');

const app = express();

app.use(express.json());

app.use('/', routes.sseRoute);

app.listen(8082, () => {
  console.log('Server is starting.');
});
